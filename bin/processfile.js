var lf4d = require('../src/loadfile4dom');
var fs = require('fs');
var path = require('path');
//var vFileName = path.join(__dirname,'tests/cache/africaans.txt');
var vDefaultFile = './tests/files/default.js';
var vFileName = "";

console.log("ARG-PARAMETER: ("+process.argv.join(",")+")");
var args = process.argv.slice(2, process.argv.length);
console.log("ARGS: ("+args.join(",")+")");
var modes = {
  '--json': 'json',
  '--plaintext': 'text',
  '--html': 'html',
  '--markdown': 'markdown',
  '--reveal': 'reveal',
  '--latex': 'latex',
};
var mode = 'text';
if (args.length < 1) {
  console.log("ERROR: missing parameter\n   node bin/processfile.js --<mode> filename\n   modes: --ast --js  --nodetypes ");
} else if (args.length < 2) {
  vFileName = args[0];
  console.log("CALL: processfile.js with default mode '--" + mode + "' and treat first parameter as filename.");
} else {
  // processfile is called with less than 3 parameters
  // set the missing parameters with default values
  if (modes.hasOwnProperty(args[0]) === true) {
    mode = modes[args[0]];
    vFileName = args[1];
    console.log("(2) MODE: procesfile.js Mode was set to '"+mode+"'");
  } else {
    mode = "text";
    vFileName = args[1];
    console.log("(2) MODE: processfile.js Mode use default mode '"+mode+"'");
  }
  console.log("FILE: processfile.js Mode set file to '"+vFileName+"'");
}
// read filename and apply wtf() on wiki data
fs.readFile(vFileName, {encoding:"utf-8"}, function(err,data){
  if (!err) {
    //console.log('Received Data from "'+vFileName+'":\n'+data);
    //console.log(wtf(data).text());
  } else {
    console.log("ERROR '"+vFileName+"':\n: "+err);
  }
});
