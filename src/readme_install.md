

## Installation `LoadFile4DOM`
There are two main types to use `LoadFile4DOM` for you projects. With a `script`-tag in your HTML file or with a package manager like [NPM](https://www.npmjs.com/) with [NodeJS]()
### Installation `LoadFile4DOM` with NPM for Scripts
Assume you have NPM installed and your have created e.g. a folder `mypackage/` for your package with `package.json` in the folder `. Go to the folder `mypackage/` and call
```javascript
npm install loadfile4dom --save
```
Then you will find `loadfile4dom` in the folder `mypackage/node_modules/loadfile4dom`.
If you want to use `LoadFile4DOM` in your scripts use the following require-call:
```javascript
const  LoadFile4DOM = require('loadfile4dom');
```
Now it is possible to use `LoadFile4DOM` in your scripts.
### Installation `LoadFile4DOM` for Browser for Scripts-Tags
If you want to use the library `loadfile4dom.js` in a browser, please copy the file `dist/loadfile4dom.js` into your library folder (e.g. `/js`) and
import the library with `script`-tag with:
```html
<script src="js/loadfile4dom.js"></script>
```
Now it is possible to use `LoadFile4DOM` in your other imported scripts.
