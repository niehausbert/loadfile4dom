## NPM Library Information
* Exported Module Variable: `LoadFile4DOM`
* Package:  `loadfile4dom`
* Version:  `1.2.19`   (last build 2021/10/14 11:32:13)
* Homepage: `https://gitlab.com/niehausbert/loadfile4dom#readme`
* License:  MIT
* Date:     2021/10/14 11:32:13
* Require Module with:
```javascript
    const vLoadFile4DOM = require('loadfile4dom');
```
* JSHint: installation can be performed with `npm install jshint -g`
