/* ---------------------------------------
 Exported Module Variable: LoadFile4DOM
 Package:  loadfile4dom
 Version:  1.2.19  Date: 2021/10/14 11:32:13
 Homepage: https://gitlab.com/niehausbert/loadfile4dom#readme
 Author:   Engelbert Niehaus
 License:  MIT
 Date:     2021/10/14 11:32:13
 Require Module with:
    const LoadFile4DOM = require('loadfile4dom');
 JSHint: installation with 'npm install jshint -g'
 ------------------------------------------ */

/*jshint  laxcomma: true, asi: true, maxerr: 150 */
/*global alert, confirm, console, prompt */
