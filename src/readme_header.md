# LoadFile4DOM
`LoadFile4DOM` is a library that allows to load files into an application that run completely in a browser without the need to submit data to a server for processing. With this library the users are able load files into your browser application and process the data in the browser and provide the output to the user, without submitting any data to a server.
* **[Demo LoadFile4DOM](https://niehausbert.gitlab.io/loadfile4dom)**
