
## Libraries required for  `LoadFile4DOM`
The following libraries are necessary for `loadfile4dom.js`:


## Libraries for Building and Developement
The following libraries are necessary for building the `loadfile4dom`. 
These libraries are not included in `loadfile4dom.js`, but e.g. are required in `build.js`.
* Lib: `build4code` Version: `^0.3.13`
* Lib: `concat-files` Version: `^0.1.1`
* Lib: `doctoc` Version: `^1.3.0`
* Lib: `jsdom` Version: `^13.1.0`
* Lib: `shelljs` Version: `^0.8.3`
* Lib: `uglify-js` Version: `^3.6.0`

